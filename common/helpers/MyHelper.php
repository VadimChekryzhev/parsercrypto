<?php

namespace common\helpers;

use yii\web\Controller;

class MyHelper extends Controller
{

  public static function debug($arr)
  {
    echo '<pre>'. print_r($arr, true) . '</pre>';
  }

}